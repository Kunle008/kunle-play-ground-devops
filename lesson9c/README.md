The ami is manually created from an instance on aws with 
- centos image
- nginx installed remotely using ansible-playbook with yaml file
- filebeat for loggint monitoring was installed manually and configured to send logs

the following were modified in the filebeat.yaml file

filebeat.inputs:
- type: log
  enabled: true
  paths:
    - /var/log/*
filebeat.config.modules:
  path: ${path.config}/modules.d/*.yml
  reload.enabled: false
setup.template.settings:
  index.number_of_shards: 3
setup.kibana:
output.elasticsearch:
  hosts: ["IPOFLOGSERVER:9200"]
processors:
  - add_host_metadata: ~
  - add_cloud_metadata: ~`