variable "region" {
  description = "AWS region" 
  default     = "eu-west-1"
 }

variable "ami_id" {
  description  = "kunle-cs-104-custom_ami"
  default      = "ami-0084342368c67e045"
 }

 variable "account_ids" {
   description = "List of Acccount IDs"
   type = list
   default = ["984463041714","023451010066","197064613889"]
 }

