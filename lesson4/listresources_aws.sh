ws resources usage report from bastion server
# Author: kunle solanke
# Website: bastion-node.dev-minds.com
## format:  Hostname, DateTime, CALLERID%, VPCs%, ORGANISATION%, SUBNETPERPVC1%, SUBNETPERPVC2%, IAMUSER%, S3BUCKET%, EC2LIST%, ROUTE53HOSTED% 
Hostname=$(hostname)
DATET=$(date "+%Y-%m-%d %H: %M: %S") 
CALLERID=$(aws sts get-caller-identity)
VPCs=$(aws ec2 describe-vpcs --output table)
ORGANISATION=$(aws organizations describe-organization)
SUBNETPERPVC1=$(aws ec2 describe-subnets \
    --filters "Name=vpc-id,Values=vpc-0da06b70c407306d4"
    -- output table)

SUBNETPERPVC2=$(aws ec2 describe-subnets \
     --filters "Name=vpc-id,Values=vpc-0ff96bba8e588c79d" q
     --output table)
IAMUSER=$(aws iam list-users --profile default --output table)
S3BUCKET=$(aws s3 ls --profile default --output table)
EC2LIST=$(aws ec2 describe-instances --output table)
ROUTE53HOSTED=$(aws route53 list-hosted-zones --output table)


   echo  "'HostName, DateTime, CALLERID(%), VPCs(%), ORGANISATION(%), SUBNETPERPVC1(%), SUBNETPERPVC2(%), IAMUSER(%), S3BUCKET(%), EC2LIST(%), ROUTE53HOSTED(%)'"
   echo -e "$HOSTNAME, $DATET, $CALLERID, $VPCs, $ORGANISATION, $SUBNETPERPVC1, $SUBNETPERPVC2, $IAMUSER, $S3BUCKET, $EC2LIST, $ROUTE53HOSTED"
