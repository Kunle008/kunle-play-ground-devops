#!/bin/bash
aws ec2 create-security-group --group-name kunlesecgrp --description KunlesecG --vpc-id vpc-51882128

aws ec2 authorize-security-group-ingress --group-id sg-025268ab0ac294388 --protocol tcp --port 22 --cidr "0.0.0.0/0"

aws ec2 run-instances --image-id ami-0b850cf02cc00fdc8 --count 1 --instance-type t2.micro --key-name "kunlepersonal" --security-group-ids sg-025268ab0ac294388 --subnet-id subnet-5dd5f107 --placement  AvailabilityZone="eu-west-1a"

aws ec2 create-tags --resources i-0df80ca3ae8b7bd52 --tags Key=Name,Value=cs-104-lesson6-kunle
