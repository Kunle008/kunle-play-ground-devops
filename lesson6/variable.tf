variable "aws_ami" {
  default = ""
}

variable "server_type" {
  default = ""
}	

variable "target_vpc" {
  default     = ""
  description = "DevMinds Default VPC: aws acct as default"
}

variable "target_subnet" {
  default     = ""
  description = "DevMinds Default VPC SN: eu-west-1"
}

variable "target_keypairs" {
  default     = ""
  description = "DevMinds default keys:"
}	
