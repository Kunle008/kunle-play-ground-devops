 # EC2 Server Definition
  resource "aws_instance" "EC2InstanceDevops" {
  ami                    = "${var.aws_ami}"
  instance_type          = "${var.server_type}"
  key_name               = "${var.target_keypairs}"
  subnet_id              = "${var.target_subnet}"
  vpc_security_group_ids = [aws_security_group.webserver_sg.id]
 
  provisioner "remote-exec" { 
  connection {
      type    = "ssh"
       host = "${self.public_ip}"
      user    = "centos"

      private_key = "${file("/home/kunle/scripts/cs-104-kunle/lesson6/kunlepersonal.pem")}"
    timeout     = "6m"
    }
 inline = [
  "sudo yum -y update",
  "sudo yum  -y install epel-release",
  "sudo yum -y install nginx",
  "sudo systemctl -l enable nginx",
  "sudo systemctl -l start nginx",
  "sudo yum -y install wget, unzip",
  
  ]

  
  }

  
  tags = {
    Name        = "cs-104-lesson6-KUNLE"
    Environment = "UNIT TEST"
    App         = "React App"
  }
}

output "pub_ip" {
  value      = ["${aws_instance.EC2InstanceDevops.public_ip}"]
  depends_on = [aws_instance.EC2InstanceDevops]
} 
