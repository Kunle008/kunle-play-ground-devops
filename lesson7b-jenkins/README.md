WARNING NOTE***
In the iam directory,once created,NO terraform destroy should be executed

7b-jenkins
===========
This project create a self-service solution for bucket creation and AWS IAM service-account creation for teams to easily use and 
create them selves bucket storage and serviceaccounts they will use for their upcoming project in the AWS 
and give flexibility to deploy to 3 different accounts.

but first,
assume(switch) roles(access) to multiple accounts

A. Gain access to multiple other accounts via GUI 

B. Gain access to multiple other accounts via CommandLine 

 

A. How to switch roles via GUI to multiple access 
-Login to account with your current credentials (Login|Password)
SPEPS:
# Click on user drop down 

# Click on switch roles 

#Provide all details in box
-account
-Role
-Display NameSet a color of choice per roles 

# Then finally click switch roles 

# This will get you to individual accounts 

REPEAT THE STEPS FOR ALL ACCOUNTS TO BE ADDED

B. How to switch roles via CLi to multiple access 
Assuming your aws credentials has been configured, now we need to use that single aws credentials you already have to assume roles across 
to other accounts. 

Confirm credentials file is in place on your account
# ll .aws
IF NOT,RUN
aws configure and add access and secret keys ,region and format(json-preferable)
# Now vim  into ~/.aws/config file and modify it like below(SET PROFILES)

[source]
region = eu-west-1
output = json



[destination]
region = eu-west-1
output = json


[profile sandbox-kunle]
role_arn = arn:aws:iam::acct1NO here:role/OrganizationAccountAccessRole
source_profile = destination

[profile preprod-kunle]
role_arn = arn:aws:iam::acct2NO here:role/OrganizationAccountAccessRole
source_profile = destination


[profile prod-kunle]
role_arn = arn:aws:iam::acct3NO here:role/OrganizationAccountAccessRole
source_profile = destination
     
# Now vim  into ~/.aws/credentials file and modify it like below

[source]
aws_access_key_id = personal key
aws_secret_access_key = personal key

[destination]
aws_access_key_id = destination main acct acc key
aws_secret_access_key = destination main acct secret key


TEST
Now test each profile that is set on the ~/.aws/config to query separate accounts using

$ aws ls --profile {profilename}   
as declared in the config file for each account
This will give you information on whats already on each profile
