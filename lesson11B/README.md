To deploy app
=============

kubectl apply -f deployment.yaml
kubectl apply -f service.yaml

kubectl get pods
kubectl get pods -watch
kubectl describe po <NAME>
kubectl get service    -->(this will get the NodePort allocated to the pod)
minikube service shopizer-service  -->This gives you the url and portNo to test application on browser if its exposed through Nodeport)


localhost:portNo will run app on browser because its being tested locally on minikube