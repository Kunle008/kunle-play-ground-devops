 # EC2 Server Definition
 resource "aws_instance""Devops" {
  ami                    = var.aws_ami
  instance_type          = var.server_type
  key_name               = var.target_keypairs
  subnet_id              = var.target_subnet
  vpc_security_group_ids = [aws_security_group.webserver_sg.id]

  provisioner "remote-exec" {
  connection {
      type    = "ssh"
       host = self.public_ip
      user    = "centos"

      private_key = file("/home/kunle/cs-104-kunle/lesson9b/kunlepersonal.pem")
    timeout     = "6m"
    }
 inline = [
  "sudo yum -y update",
  "sudo yum  -y install epel-release",
  "sudo yum -y install nginx",
  "sudo systemctl -l enable nginx",
  "sudo systemctl -l start nginx",
  "sudo yum -y install wget, unzip",]


  }

provisioner "local-exec" {
    command = "sleep 30; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u centos -i '${self.public_ip},' --private-key kunlepersonal.pem r_site.yml"
  }
tags = {
    Name        = "cs-104-lesson9b-kunle"
    Environment = "DEV"
    App         = "NGINX"
  }  
}

output "pub_ip" {
  value      = ["${aws_instance.Devops.public_ip}"]
  depends_on = [aws_instance.Devops]
}
